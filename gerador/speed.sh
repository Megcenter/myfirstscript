clear
cd /root
echo ""
msg -bar
echo -e "   \033[1;32mCORRIENDO TEST DE VELOCIDAD ...\033[0m"
speedtest --share > speed
echo ""
echo -e "   \033[1;34mRESULTADO: \033[0m"
png=$(cat speed | sed -n '5 p' |awk -F : {'print $NF'})
down=$(cat speed | sed -n '7 p' |awk -F :  {'print $NF'})
upl=$(cat speed | sed -n '9 p' |awk -F :  {'print $NF'})
lnk=$(cat speed | sed -n '10 p' |awk {'print $NF'})
msg -bar
echo -e "\033[1;32mPING (LATENCIA):\033[1;37m$png"
echo -e "\033[1;32mDOWNLOAD:\033[1;37m$down"
echo -e "\033[1;32mUPLOAD:\033[1;37m$upl"
echo -e "\033[1;32mLINK: \033[1;37m$lnk\033[0m"
msg -bar
rm -rf /root/speed
echo -e " "
echo -e "\033[1;31mPRESIONE ENTER PARA CONTINUAR\033[0m"
read -p " "
menu